package nudo

import (
	"fmt"
	"github.com/mitchellh/go-homedir"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
)

// Provide Sudo with a slice of strings and it will run it with admin privllages
// On Linux and Mac, it just provides the arguments to sudo
// On Windows, it will create a powershell instance as admin And run the commands that way
// All other operating systems are currently unsupported
func Sudo(args []string) error {
	if len(args) == 1 {
		fmt.Println("usage: nudo [command]")
		return nil
	}

	switch runtime.GOOS {
	case "darwin":
		fallthrough
	case "linux":
		err := unixSudo(args[1:])
		return err
	case "windows":
		err := windowsSudo(args[1:])
		return err
	default:
		return fmt.Errorf("Operating system %s not supported\n", runtime.GOOS)
	}
	return fmt.Errorf("You shouldn't be able to see this message\n")
}

// Tries to guess username based on home directory
// Uses github.com/mitchellh/go-homedir
func User() (string, error) {
	s, err := HomeDir()
	if err != nil {
		return "", err
	}

	return filepath.Base(s), nil
}

// Uses github.com/mitchellh/go-homedir
func HomeDir() (string, error) {
	return homedir.Dir()
}

func windowsSudo(args []string) error {
	// Start-Process powershell -ArgumentList '-noprofile [command]' -verb RunAs
	nargs := []string{"Start-Process powershell -ArgumentList", "", " -verb RunAs"}
	command := "'-noprofile " + strings.Join(args, " ") + "'"

	// Just in case the argument number to the empty string is incorrect
	if nargs[1] != "" {
		return fmt.Errorf("ArgumentList not empty")
	}
	nargs[1] = command
	cmd := exec.Command("powershell.exe", nargs...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func unixSudo(args []string) error {
	cmd := exec.Command("sudo", args...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}
