package main

import (
	"fmt"
	"gitlab.com/ComicSads/nudo/nudo"
	"os"
)

func main() {
	err := nudo.Sudo(os.Args)
	if err != nil {
		fmt.Printf("nudo: %s\n", err)
		os.Exit(1)
	}
}
